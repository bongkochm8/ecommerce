const express = require("express");
const productRoutes = require("./product.route");
const uploadRoutes = require("./upload.route");

const router = express.Router();

router.use("/product", productRoutes);
router.use("/upload", uploadRoutes);

module.exports = router;
