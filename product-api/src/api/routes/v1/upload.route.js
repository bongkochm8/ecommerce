const express = require("express");
const controller = require("../../controllers/upload.controller");
const { authorize, LOGGED_USER, ADMIN } = require("../../middlewares/auth");
const router = express.Router();

router.route("/").post(authorize([ADMIN, LOGGED_USER]), controller.upload);

module.exports = router;
