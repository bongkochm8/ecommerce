const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/product.controller");
const { authorize, LOGGED_USER, ADMIN } = require("../../middlewares/auth");
const {
  createProduct,
  listProducts,
} = require("../../validations/product.validation");
const router = express.Router();

module.exports = router;

router
  .route("/")
  /**
   * @api {get} v1/products List Products
   * @apiDescription Get a list of products
   * @apiVersion 1.0.0
   * @apiName ListProducts
   * @apiGroup Product
   * @apiPermission 
   *
   * @apiHeader {String} Authorization   User's access token
   *
   * @apiParam  {Number{1-}}         [page=1]     List page
   * @apiParam  {Number{1-100}}      [perPage=1]  Products per page
   * @apiParam  {String}             [search]     search name product

   *
   * @apiSuccess {Object[]} products List of products.
   *
   * @apiError (Unauthorized 401)  Unauthorized  Only authenticated users can access the data
   */
  .get(authorize([ADMIN, LOGGED_USER]), validate(listProducts), controller.list)
  /**
   * @api {post} v1/products Create Product
   * @apiDescription Create a new product
   * @apiVersion 1.0.0
   * @apiName CreateProduct
   * @apiGroup Product
   * @apiPermission admin
   *
   * @apiHeader {String} Authorization   User's access token
   *
   * @apiParam  {String}                name        Product's name
   * @apiParam  {String}                thumbnail   Product's thumbnail
   * @apiParam  {Number}                amount      Product's amount
   * @apiParam  {Number}                price       Product's price
   * @apiParam  {Object[{String}]}      images      Product's images

   *
   * @apiSuccess (Created 201) {String}  id                 Product's id
   * @apiSuccess (Created 201) {String}  name               Product's name
   * @apiSuccess (Created 201) {String}  thumbnail          Product's thumbnail
   * @apiSuccess (Created 201) {String}  amount             Product's amount
   * @apiSuccess (Created 201) {String}  price              Product's price
   * @apiSuccess (Created 201) {Object[{String}]}  images   Product's images
   * @apiSuccess (Created 201) {Date}    createdAt          Timestamp
   *
   * @apiError (Bad Request 400)   ValidationError  Some parameters may contain invalid values
   * @apiError (Unauthorized 401)  Unauthorized     Only authenticated users can create the data
   * @apiError (Forbidden 403)     Forbidden        Only admins can create the data
   */
  .post(authorize(ADMIN), validate(createProduct), controller.create);

router
  .route("/:productId")
  /**
   * @api {get} /v1/product/:id Get Product
   * @apiDescription Get Product  information
   * @apiVersion 1.0.0
   * @apiName GetUser
   * @apiGroup User
   * @apiPermission user
   *
   * @apiHeader {String} Authorization   User's access token
   *
   * @apiSuccess (Created 201) {String}  id                 Product's id
   * @apiSuccess (Created 201) {String}  name               Product's name
   * @apiSuccess (Created 201) {String}  thumbnail          Product's thumbnail
   * @apiSuccess (Created 201) {String}  amount             Product's amount
   * @apiSuccess (Created 201) {String}  price              Product's price
   * @apiSuccess (Created 201) {Object[{String}]}  images   Product's images
   * @apiSuccess (Created 201) {Date}    createdAt          Timestamp
   *
   * @apiError (Unauthorized 401) Unauthorized Only authenticated users can access the data
   * @apiError (Not Found 404)    NotFound     Product does not exist
   */
  .get(authorize(LOGGED_USER), controller.get);
