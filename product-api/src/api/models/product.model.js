const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: true,
    },
    thumbnail: {
      type: String,
      required: true,
    },
    amount: {
      type: Number,
      required: true,
      min: 1,
    },
    price: {
      type: Number,
      required: true,
      min: 1,
    },
    images: [String],
  },
  {
    timestamps: true,
  }
);

productSchema.statics = {
  list({ page = 1, perPage = 30, q = {} }) {
    // const options = omitBy({ name, email, role }, isNil);

    return this.find(q)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },
};

module.exports = mongoose.model("product", productSchema);
