const Joi = require("joi");

module.exports = {
  listProducts: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1),
      search: Joi.string(),
    },
  },
  createProduct: {
    body: {
      name: Joi.string().required(),
      thumbnail: Joi.string().required(),
      amount: Joi.number().min(1),
      price: Joi.number().min(1),
    },
  },
};
