const Product = require("../models/product.model");
const httpStatus = require("http-status");

exports.create = async (req, res, next) => {
  try {
    const product = new Product(req.body);
    const savedProduct = await product.save();
    res.status(httpStatus.CREATED);
    res.json(savedProduct);
  } catch (error) {
    next(error);
  }
};

exports.list = async (req, res, next) => {
  try {
    if (req.query.search !== undefined) {
      req.query.q = { name: { $regex: `.*${req.query.search}.*` } };
    }
    // console.log("q: ", req.query);
    const products = await Product.list(req.query);
    res.json(products);
  } catch (error) {
    next(error);
  }
};

exports.get = async (req, res, next) => {
  try {
    const product = await Product.findById(req.params.productId);
    res.json(product);
  } catch (error) {
    next(error);
  }
};
