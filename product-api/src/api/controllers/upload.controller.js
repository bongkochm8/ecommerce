const APIError = require("../utils/APIError");
const httpStatus = require("http-status");
const path = require("path");
const fs = require("fs");

const filePath = "/uploads/";
const uploadDir = path.join(__dirname, "/..", "/..", "/..", `${filePath}`);

function fileupload(file, fullpath) {
  return new Promise((resolve, reject) => {
    file.mv(fullpath, async (err) => {
      if (err) {
        const error = new APIError({
          status: httpStatus.CONFLICT,
          message: err.message,
        });
        reject(error);
      }
      resolve(fullpath);
      // return fullpath;
    });
  });
}

exports.upload = async (req, res, next) => {
  const _fileupload = fileupload;
  try {
    if (!fs.existsSync(`${uploadDir}`)) {
      fs.mkdirSync(`${uploadDir}`);
    }
    console.log("file: ", req.files);
    if (req.files === undefined || req.files.images === undefined) {
      throw new APIError({
        status: httpStatus.CONFLICT,
        message: "images required",
      });
    }
    const sampleFile = req.files.images;
    const time = new Date().getTime();
    if (sampleFile.length > 0) {
      console.log(sampleFile);
      const files = await Promise.all(
        sampleFile.map(async (val) => {
          const fullpath = `${uploadDir}${time}-${val.name.trim()}`;
          // const fullpath = `${uploadDir}${val.name}`;
          _fileupload(val, fullpath);
          return `/uploads/${time}-${val.name.trim()}`;
          // return `/uploads//${val.name}`;
        })
      );
      res.json({ success: true, files });
    } else {
      let fullpath = `${uploadDir}${time}-${sampleFile.name.trim()}`;
      const files = [];
      await sampleFile.mv(fullpath, async (err) => {
        if (err) {
          throw new APIError({
            status: httpStatus.CONFLICT,
            message: err.message,
          });
        }
        fullpath = `/uploads/${time}-${sampleFile.name.trim()}`;
        // fullpath = `/uploads/autosociety/${sampleFile.name}`;
        files.push(fullpath);
        res.json({ success: true, files });
      });
    }
  } catch (error) {
    next(error);
  }
};
