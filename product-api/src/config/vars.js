const path = require("path");

// import .env variables
require("dotenv-safe").load({
  path: path.join(__dirname, "../../.env"),
  sample: path.join(__dirname, "../../.env.example"),
});

module.exports = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
  jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,
  mongo: {
    uri:
      process.env.NODE_ENV === "test"
        ? process.env.MONGO_URI_TESTS
        : process.env.MONGO_URI,
  },
  logs: process.env.NODE_ENV === "production" ? "combined" : "dev",
  localUri: {
    order: process.env.ORDER_URI || "http://127.0.0.1:3002",
    product: process.env.PRODUCT_URI || "http://127.0.0.1:3001",
    user: process.env.USER_URI || "http://127.0.0.1:3000",
  },
};
