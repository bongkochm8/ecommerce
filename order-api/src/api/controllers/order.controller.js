const Product = require("../models/order.model");
const httpStatus = require("http-status");
const Order = require("../models/order.model");
const APIError = require("../utils/APIError");

exports.create = async (req, res, next) => {
  //   console.log("user: ", req.user);
  console.log("body: ", req.body);
  const { products } = req.body;
  req.body.userId = req.user.id;
  req.body.totalPrice = 0;
  products.map((item) => {
    req.body.totalPrice += item.price * item.amount;
  });

  const order = new Order(req.body);
  const savedOrder = await order.save();
  res.json(savedOrder);
};

exports.cancel = async (req, res, next) => {
  try {
    const order = await Order.findById(req.params.orderId);
    if (
      order.status === "verify" &&
      order.userId.toString() === req.user.id.toString()
    ) {
      order.status = "cancel";
      await order.save();
      return res.json(order);
    }

    if (order.userId.toString() !== req.user.id.toString())
      throw new APIError({
        status: httpStatus.FORBIDDEN,
        message: "access denies",
      });

    throw new APIError({
      status: httpStatus.FORBIDDEN,
      message: "Please contact support",
    });
  } catch (error) {
    return next(error);
  }
};

exports.get = async (req, res, next) => {
  try {
    const order = await Order.findById(req.params.orderId);
    if (
      order.userId.toString() === req.user.id.toString() ||
      req.user.role === "admin"
    ) {
      return res.json(order);
    }
    throw new APIError({
      status: httpStatus.FORBIDDEN,
      message: "access denies",
    });
  } catch (error) {
    return next(error);
  }
};

exports.list = async (req, res, next) => {
  try {
    if (req.user.role !== "admin") {
      req.query.q = { userId: req.user.id.toString() };
    }
    const orders = await Order.list(req.query);
    res.json(orders);
  } catch (error) {
    next(error);
  }
};
