const Joi = require("joi");

module.exports = {
  listOrder: {
    query: {
      page: Joi.number().min(1),
      perPage: Joi.number().min(1),
    },
  },
  cancelOrder: {
    params: {
      orderId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
  },
  getOrder: {
    params: {
      orderId: Joi.string()
        .regex(/^[a-fA-F0-9]{24}$/)
        .required(),
    },
  },
  createOrder: {
    body: {
      name: Joi.string().required(),
      tel: Joi.string().required(),
      address: Joi.string().required(),
      products: Joi.array()
        .items(
          Joi.object().keys({
            id: Joi.string()
              .regex(/^[a-fA-F0-9]{24}$/)
              .required(),
            name: Joi.string().required(),
            amount: Joi.number().required(),
            price: Joi.number().required(),
          })
        )
        .required(),
    },
  },
};
