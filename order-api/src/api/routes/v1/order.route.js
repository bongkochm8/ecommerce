const express = require("express");
const validate = require("express-validation");
const controller = require("../../controllers/order.controller");
const { authorize, LOGGED_USER, ADMIN } = require("../../middlewares/auth");
const {
  createOrder,
  cancelOrder,
  getOrder,
  listOrder,
} = require("../../validations/order.validation");
const router = express.Router();

router
  .route("/")
  .get(authorize([LOGGED_USER, ADMIN]), validate(listOrder), controller.list)
  .post(authorize(LOGGED_USER), validate(createOrder), controller.create);

router
  .route("/:orderId")
  .get(authorize([ADMIN, LOGGED_USER]), validate(getOrder), controller.get)
  .put(
    authorize([LOGGED_USER, ADMIN]),
    validate(cancelOrder),
    controller.cancel
  );

module.exports = router;
