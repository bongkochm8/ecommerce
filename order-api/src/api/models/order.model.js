const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    tel: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      required: true,
    },
    paymentRef: {
      type: String,
    },
    paymentStatus: {
      type: String,
      enum: ["pending", "success", "error"],
      default: "pending",
    },
    status: {
      type: String,
      enum: ["verify", "process", "success", "cancel"],
      default: "verify",
    },

    transportRef: {
      type: String,
    },
    totalPrice: {
      type: Number,
      required: true,
    },
    products: [
      {
        id: String,
        name: String,
        amount: Number,
        price: Number,
      },
    ],
  },
  {
    timestamps: true,
  }
);

OrderSchema.statics = {
  list({ page = 1, perPage = 30, q = {} }) {
    // const options = omitBy({ name, email, role }, isNil);

    return this.find(q)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },
};

module.exports = mongoose.model("order", OrderSchema);
